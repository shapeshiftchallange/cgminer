# cgminer

   This is a project that builds a docker container/image using a traditional Dockerfile
   
   Compiled the Cg Miner from Source from [ckolivas/cgminer](https://github.com/ckolivas/cgminer)
   Then saved the compiled code inside this project.
  


# Build Docker
```
 docker build -t ohallord/cgminer .
```

# Deploy ansible cotainer
   
   Pushing to [Docker hub](https://hub.docker.com/) after you have logged into your Docker Hub account
   
   ```
      docker loing -u username -p 
   ```

```
 docker push ohallord/cgminer
```


## Enviroment Variables passed in to Docker container
   
   This example shows you are using [slushpol](https://slushpool.com/home/)
    
```   
MINER_USER = <your.email@address.com>
MINER_PASSWD = worker1
       worker1 --> will work for use with slushpool.com
POOL_URL --> stratum+tcp://us-east.stratum.slushpool.com:3333
```

#### Example running the docker container
```
docker run -dti --restart=on-failure:10 -e "MINER_USER=user@email.com" -e "MINER_PASSWD=worker1" -e "POOL_URL=stratum+tcp://us-east.stratum.slushpool.com:3333" 
```

## Continous Integration/Build

Using bitbucket's build pipe line and that is in the yaml file bitbucket-pipelines.yml